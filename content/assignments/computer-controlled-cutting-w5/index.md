+++
    Title = "Computer Controlled Cutting - Week 5"
    weight = 50
+++

### Introduction
This week I wanted do something a bit different then my final project. I wanted to test out snap fit pieces concept. 
When I was talking to my partner I noticed that he needs some paint boxes for his miniature painting hobby. So the idea was some sort of container that is inspired from below picture but simpler and more modular.

![Inspiration](inspiration.jpg)

For this I took measurements of the paint boxes below to be entered into Fusion 360 as user defined parameters.

|![Paints](paints.jpg)|![Parameters](parameters.jpg)|
---|---

My idea was to make box with finger joints that'll snap in. Then the top plane of the box would have a circular hole for inserting the paints through (which would be able to contain the widest paint bottle as well). The fornt window would be fone with acrylic for seeing the paint names and the sides will be done with plywood of the same thickness. The sides of the boxes would be long enough to hit the neck of the shortest paint at least to prevent paints from tipping around when used within the container. I also measured the opening distance of the caps to make sure they fit whitin the design. There would be a bottom tray where the individual boxes could be snap fit to and can be moved if needed.

### Steps
#### Kerf
I first wanted to experiment with the machine and see the effect of different laser parameter effects on kerf. This is why I first experimented on cardboard, where me and Elise first tried the power of the machine in terms of cutting and engraving and its effect on kerf. Kerf test is basically figuring out what the cutting error is due to the size of the laser beam. Due to beam diameter we will always cut a bit smaller than we draw, therefore we should take kerf into account when we are drawing our piece that needs to be snap fit or fit a specific geometry in it. Kerf is done by taking 5 known size samples of squares, cutting them, measuring the length of all, substracting that from all and dividing it to sample size 5. This final value is called kerf.

Here are a picture and a table below about our test results:

![ Engraving and Kerf Results](engraving_and_kerf.jpg)

|**Power**|50%|75%|100|
----|----|-----|----
|**Kerf**|0.14|0.06|0.14|

We figured out engraving with 75% power was the best visibility result for us (from bottom to top 50%, 75% and 100%). When we tried the same cutting power trials on cardboard, we noticed that there is a chance of getting different kerf values with different power values, although the extend of this difference is inconclusive due to the bendeble nature of the cardboard.

After this test we also learned quite well have to operate the laser cutter. I moved onto my design with two different materials as 3 mm plywood and 3 mm acrylic after this point. I carried out Kerf test for these as well, after deciding the best cutting settings with the laser cutter. Here I would recommend only cutting one square until you figure out the best settings to avoid waste.

Here are the laser cutter settings I settled on for my cuts.

||Plywood(3mm)|Acrylic(3mm)|
---|---|---
|**Speed**|20%|18%|
|**Power**|100%|100%|
|**Frequency**|10%|100%|

Below are the cut squares for the kerf test.

|![Plywood Kerf](plywood_kerf.jpg)|![Acrylic Kerf](acrylic_kerf.jpg)|
----|----

Here are the kerf results:
||Plywood(3mm)|Acrylic(3mm)|
---|---|---
|**Kerf**|0.16mm|0.14mm|

#### Designing in Fusion 360
##### Top and bottom pieces/pleanes of the box
    - Start with a center point rectangle (make its size: (kerf_plywood + clearance x 2 + bottom_diameter + opening_additional_thickness x 2) BY (kerf_plywood + clearance x 2 + bottom_diameter + opening_additional_thickness x 2)).

![Center point reactangle](center_point_rectangle.jpg)

    - Then make a line with the height of *material_thickness* at the top left corner, then another line with the width of the *bottom_finger_size*, then a final line to close this to a reactangle.

![Finger](finger.jpg)

    - Then use this finger and the rectangular pattern tool (distance: ) to create three more of these on the same line. I decided to do 4 of these fingers and int he bottom and top planes I decided to have 6 mm width for these fingers. After removing the size of these (6mm*4) from the total length of the piece and divide it to three(4-1) we can figure out the *finger_distance_size* which could be also stored as a parameter.

![Rectangular Pattern Tool](reactangular_pattern_tool.jpg)

    - After this use the mirror tool, diagonal construction lines of the big rectangle and the 4 fingers to create the fingers on all the sides of the rectangle. Here is the final piece of top and bottom planes.

![Bottom and top pieces](bottom_and_top_piece.jpg)

    - I actually forgot to include the circle for the top plane, therefore added this later in Adobe Illustrator.

##### Back and front pieces/planes of the box
    - First of all this pieces needs to fit with the bottom and top pieces, meaning their top and bottom needs to be complementary to the boxes top and bottom pieces.
    - For the sides I decided to put the fingers starting from the corner again, the hegiht of this side of the rectangle is decided from the *neck_height* variable as (neck_height + kerf). Here kerf will be different for front (acrylic 3mm) and back (plywood 3mm), therefore it is smart to draw them as two sketches with kerf_plywood and kerf_acrylic parameters.
    - I decided to have 3 fingers on the sides with the side_finger_size 4 mm and the distance between can be calculated as discussed in the previous case.
    - At this point I noticed a major mistake I did, which was completely forgetting about the length change due to different kerfs and its effect to the finger distances, therefore I now parameterized all of the finger ditances as well. (I also updated the parameter list image)
    - I used the same steps explained above with the side piece measurements, the only difference being I added an additional *material thickness* to the bottom parts of front/back/left/right pieces to enable them to connect to the bottom tray.
    - The below picture shows this for acrylic piece (front) and the plywood back piece.
    
|![Acrylic front and plywood back pieces](acrylic_front.jpg)|![Acrylic front and plywood back pieces](acrylic_front.jpg)|
----|----

##### Left and right pieces/planes of the box
    - These left and right pieces will have complementary fingers on both bottom/top and left/right.
    - They also needed additional *material thickness* to the bottom parts of front/back/left/right pieces to enable them to connect to the bottom tray.
    - Here are the final pictures:

![Plywood left and right pieces](left.jpg)

##### Tray
    - To start with one can draw the same shape as the bottom piece but the complementary version of it in terms of fingers.
    - Then add an additional concentric rectangle with larger size (larger by material thickness x 2 BY material thickness x 2).
    - Make all the lines that does not need to be cut a construction line.
    - Mirror this as many times as you want in X and Y directions. I did a 2 by 2 grid.
    - Make the lines between these grids construction lines as well.
    - Here is the final picture:

![Plywood tray piece 2 by 2](tray.jpg)

#### Exporting to DXF and manipulations in Adobe Illustrator
    - After all is done, I selected each component separately and their sketch, right clicked and selected save as DXF file.
    - Then I opened the Adobe Illustrator and selected any canvas. Set the units to mm.
    - From edit menu selected *place* command. Here also selected the option that allows you to see more settings for importing.
    - When placing, AI will ask you what kind of scaling would you like to use. Here I selected original size and scale the linework as well.
    - Then placed the piece anywhere you want on the page.
    - Selected the piece and ungrouped for removing any construction lines that are left.
    - Also one can use the remove points or connect anchor points options within the Illustrator to complete the removal of the construction lines properly.
    - After this I grouped the whole shape again, then slected the stroke width to 0.01 mm. This setting is important because it tells laser cutter that this is a piece to be cut rather than engraved or ignored.
    - When editing is finished I saved the file for future use and transfer.

#### Starting up the laser cutter and manual focusing
    - I opened the ventilator to max before starting to work, if it is not already on. It seems that this button is actually representing hours not power.
    - Then wait a bit and turn on the laser cutter by reading the card at the desk, pressing ok and then finally also switching the manual key on the machine.
    - After this wait until the screen loads.
    - Load the material to laser cutter, use the machine screen to select the move menu and use the joystick to bring the laser head to position. It is recommended to do the focus adjustment for left bottom corner.
    - Drop the magnetic piece down and use the focus menu on the screen to adjust it to a perfect perpendicular position without any bending, also having a mini tension but not too much on the material.
    - After this put the magnetic piece back up and close the machine mouth.
    - Optionally use the move screen to move the laser head out of the way of the cameras.
    - If the cameras are functioning fully (one can see this job manager), you can proceed to cutting.

#### Job manager and laser cutter
    - When you press print in AI in laser cutter computer, the Epilog Laser Cutter comes automatically as an option. Make sure this is selected. If one used their own computer for editing in AI, they should pass the file to laser cutter computer with a USB flash disk and open in AI there.
    - After this step the epilog printer software comes up.
    - Here one should position the pieces, if wanted the copies can be created here as well. The scaling is not advised for cut fit accuracy in this window.
    - Then one should select the material and the cutting settings. According to the line widths set the software will automatically recognize the options as either engraving or cutting.
    - After selecting the correct material, adjust the cut settings: speed, power, frequency. The recommended values for the selected materials will automatically load but you might need to change these a bit depending on your test pieces.
    - After everything is set press send to job manager or directly print. If sent to job manager, select jobs and click on the correct job and select quick print.
    - These steps send the job to the machine screen, here select the correct job and press play button to start the cut.
    - You can see the estimated time on the machine screen.
    - Here is a video of the process:

{{< video src="laser_cutter.mp4" type="video/mp4" preload="auto" >}}

### Results
- Unfortunately I did not get the snap fit I wanted to do in one go, the reason for this was having two materials but not parameterizing the model completely at the beginnning as well as changing to a acrylic piece with stickers from one without.
- Another mistake I did at first trial was I actually draw the wrong holes in the Fusion 360 for the tray.
- Even though I had these mishaps, I was able to improve my parametric design, correct the drawings and kerf values to get a better second fit.
- I would still say this is not a snap fit but rather okay fit that might require a bit of glueing. 
- Here are my final pieces after trial two and them assabled together:

|![Kit pieces](kit_pieces.jpg)|![Kit assambled](kit_assambled.jpg)|![Kit swappable](kit_swappable.jpg)|
---|---|---

### Conclusions
- I would say the important thing in laser cutting is to design everything fully parametric to allow easy changes.
- I would definetely recommend trying materials of different thicknesses and figure out their cutting behaviour and kerf with the laser cutter first before trying anything large. 
- I recommend cutting one corner fit to get the kerf even better optimized before printing a whole box.
- I would recommend not using any extrusions in Fusion 360 to draw for laser cutting. Just use different component sketches and keep if fully constrained in order to make sure you can get the parametric updates when needed. Later modify the DXF file or create a second sketch based ont he first to remove the construction lines before exporting in order to remove the unnecessary lines that will show in Adobe Illustrator.
- I learned a lot this week: how to use the laser cutter, how to calculate kerf, how to use Fusion 360 better (mirror and rectangular pattern functions), how to draw finger joints, how to use a bit of AI, how plywood, cardboard and acrylic reacts under different laser cutting conditions and the resulting product characteristics. 
- I noticed that laser cutter can sometimes say silly things or the camera vision can disappear, but just restarting the machine helps.
- Do not forget to put the ventilation on and don't leave the laser cutter unattended.

 