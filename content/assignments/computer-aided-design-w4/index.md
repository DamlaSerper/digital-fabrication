+++
    Title = "Computer-Aided Design - Week 4"
    weight = 40
+++

Hello!

### Designing/drawing something in FUSION 360
#### Tutorials
This week I decided to focus on Fusion 360 learning. I was interested in Fusion 360 because I have used it before but with my self learning. After watching some Fusion tutorial video series I noticed I was missing the following knowledge:

- How to constrain things and what does this do?
- How to use parameters within Fusion 360?
- What are components and how are they different from bodies?
- How does fillet work?
- How the press-pull function works in Fusion 360?
- How to project sketches on a surface of a body as a new sketch?
- What are the different types of extrusion options (I only used basic one before)?

Here are the links for the video tutorial series I followed.
- https://help.autodesk.com/view/fusion360/ENU/courses/AP-BODIES-COMPONENTS-GS
- https://help.autodesk.com/view/fusion360/ENU/courses/AP-BODIES-COMPONENTS
- https://help.autodesk.com/view/fusion360/ENU/courses/AP-OPEN-CLOSE-EXPORT-UPLOAD-SAVE
- https://help.autodesk.com/view/fusion360/ENU/courses/AP-SKETCH-FUNDAMENTALS

I did not yet go too much into 3D functionality of Fusion except extrusion, although I am planning on learning on this with more tutorials in the upcoming weeks.

#### First idea
After watching these videos I had two projects to try in mind. First one that I decided to make was three 1x1x0.5 cm rectangular prisms from Formlabs Tough 1500 resin that is used in Formlabs SLA printers. I know this material behaved the best with my system in the last trials but I did not check (in the lab) exactly what is the chemical resistance or hydrophobicity of the material. According to website of the material it should be somehow resistant but I discussed with a colleague of mine and decided to carry out a test with him in the lab. Therefore I printed these little prisms. 

- The modelling of this was quite straight forward with make a 1x1 cm square on X-Y plane and extrude by 0.5 cm. Here is the 2D and 3D drawing of these in Fusion 360:
|![2D sketch sample](sample_2Dsketch.jpg)|![3D sample](sample_3D.jpg)|
----|----
- Then I exported this as an .stl file.
- After this I imported this file to Formlabs PreForm Slicer software on the FABLAB computer to slice it, set printing parameters and edit the support connection points.
- I used resolution of 0.05 mm, and selected the Formlabs 3 printer that had already installed tank and Tough 1500 resin inside.
![Prefrom with supports](sample_preform.jpg)
- I printed three identical pieces, which took around 3.5 hours to complete (excluding the wash and post-cure time).
- I washed them in the wash machine that contains isopropyl alcohol for 15 min and let it sit to dry for 30 mins.
- After this I transferred the peices to the post-cure basket which uses heat and UV to cure the pieces further.
- The preferred temperature for Formlabs Tough 1500 resin is 70 degrees Celcius and 60 minutes.

Here are some images of the final pieces and the experiment solutions:
|![SLA printed samples](SLA_printed_samples.jpeg)|![Chemical resistance test solutions and samples](solutions_and_samples.jpeg)|
----|----

#### Second idea
The second project in my mind was to do with necessary geometries to be imported/transferred to digital from mechanical drawing. What I mean by this is the camera, the centrifuge and the pipe of the centrifuge. Kris ordered the Raspberry Pi camera v3. wide angle, although I could not find a readily drawn .stl file for this camera which has slightly different dimentions then v2. I could only find a mechanical drawing with dimensions. The camera needs to be roughly drawn in Fusion to make sure I can design a casing around it. 

The centrifuge itself is a constraint as well like the camera. The centrifuge is rigid and the pieces are unswappable except the inlet pipe, therefore this contrains the size, angle and shape of the case to be modelled. I don't have any digital or mechanical drawings of the centrifuge but I have done some measurments with calipers to get an as accurate as possible drawing.

Finally,the pipe is changable in diameter and its external geometry can be modified for enabling a better installed case and pipe. The dimater is contrained at 1.5 cm although this has flexibility to be adjusted along the camera case. Therefore I imagined my project in Fusion 360 as five components: 
1. Camera
2. Centrifuge
3. Pipe
    - Connection to the outer basket
    - Connection to the camera case
    - Pipe itself
    - Silicon dampeners
4. Case
    - Connection to pipe
    - Case itself
    - Polycarbonate front glass
    - Silicone waterproofing
5. LED-hat for RPi (probablt to be imported in form KiCAD)

Therefore I decided to start from drawing th camera from the mechanical drawing. I chose thiis because I can use all the new things that I have learnt in Fusion 360 and apply it within the 2D sketch mode in this camera. One thing that is good, is the extrusion tool is enough by itself to convert 2D camera sketch to 3D completely.Here are some steps describing how I drew this and the preliminary mechanical drawing:

![Mechanical drawing](camera-module-3-wide-mechanical-drawing.jpg)

- I started by drawing a 3D sketch, that is fully constraint.
    - Drew a big ractangle, snapped its center to origin, set dimensions as described in the figure.
    - Used fillet with the same dimension as the larger screw hole in the drawing, linked all other three fillet dimnesions to the first one.
    - Created the locations for mini screws by using 2 concentric circles with given dimentions and the other one being tangent to the sides of the camera.
    - I drew the mini square then changed the semi-square within it to a circle because it looked more circular in the actual ohysical camera.
    - I drew the inner most circle (the camera lens) wiht the given dimensions.
    - I used lines and contructuion lines and contraints to draw the sensor of the camera.
    - Similarly used above mentioned elements to draw the camera cable connection port.

Here is a final image of this 2D fully constraint sketch:
![2D sketch of the camera](camera_2Dsketch.jpg)

- After this sketch I extruded the parts according to the mechanical drawing with th given order below. For these sometimes I needed to create some projected sketches on the surfaces from where I would like to extrude.
    - Here is the general body extrusion from the side view (done this by selecting all the surfaces except the screw holes):
    ![General body extrusion from the side view](generalbodyextrusion.jpg)
    - Here is the front face projection:
|![Front face projection and the body](frontfaceprojectionandbody.jpg)|![Front face projection](frontfaceprojection.jpg)|
----|----
    - Here is the extruded mini quare part around the camera lens:
|![Front view](extrude_mini_square_front.jpg)|![Side view](extrude_mini_square_side.jpg)|
----|----
    - Here is the extruded circle area around the lens (same height as the square extrusion):
|![Front view](extrude_circle_front.jpg)|![Side view](extrude_circle_side.jpg)|
----|----    
    - Here is the extruded lens area (same height as the surrounding circle):
|![Front view](extrude_lens_front.jpg)|![Side view](extrude_lens_side.jpg)|
----|----  
    - Here is the front face projection:
|![Back face projection and the body](backfaceprojectionandbody.jpg)|![Back face projection](backfaceprojection.jpg)|
----|----
    - Here is the extruded cable slot:
|![Front view](extrude_cable_slot_back.jpg)|![Side view](extrude_cable_slot_side.jpg)|
----|---- 
    - Then I drew a line between the two circles (lens and the outer circle) and made circle arc to pass through those end points with a given height of 0.2 mm. Here I noticed I failed to constrain the arc unfortunately. For doing the line drawing, I first used intersect tool which is in the same tab as project and got the points that I want to use from the surface to a new sketch. Here are the projected sketch and drawing in top and front view:
|![Top view](top_projected_shape.jpg)|![Front view](front_projected_shape.jpg)|
----|----
    - I prepared a concentric circular path by creating a new sketch and using circle tool in the middle of the two circles, again used projection as well:
![Circular path top view](the_circle_path.jpg)
    - I used the sweep tool and cut the body option to cut out the arc based surface we have created along the circular path from the camera body. Here are the results:
|![Front view](sweep_front.jpg)|![Side view](sweep_side.jpg)|
----|----
    - Finally I also extruded the camera sensor from the initial projection I made to the front face of the body.Final images of the finished 3D model are below:
|![Front view](extrude_sensor_front.jpg)|![Side view](extrude_sensor_side.jpg)|
----|----
|![Top side view](final_shot_top_side.jpg)|![Bottom side view](final_shot_bottom_side.jpg)|

#### Third idea
After talking to Kris, I tried checking one more 3D modelling software called SolveSpace. In this one I tried to draw a pipe with a 3 comparments within. Here is how it went:

- I started by two concentric circles and contrained their diameter.
- I made three construction lines that are 120 degrees apart within the inner circle.

![Drawing](without_compartments.jpg)

- Then I extruded the cyclinder shell
- After this, I used the construction lines and creates 0.5 cm far away lines that are still connected to inner circle, and finally connected them to each other to get the kind of image below:

![Drawing](with_compartments.jpg)

- Then I tried extruding this to get a compartmentalized pipe, I has some minor problem here but I'll ask about it today.