+++
    Title = "Electronics Design - Week 8"
    weight = 80
+++

### Introduction
- I wanted to design RPi Camera LED HAT for this week with the layout below.

![Circuit](circuit.jpg)

- Although then I tought maybe I don't even need the switch (MOSFET or relay or button), but I can just power and control them through a GPIO pin.
- I started drawing this in KiCad with the steps below:

#### Step 1: Draw the circuit:
- I drew the circuit as four parallell LED's with resistors hooked up next to them to avoid burning the LED's.

![Step 1](Step1.jpg)

- I reduced the number of paralllel LED's later to 3 due to small footprint.
- I used Luminus 1206 LED's which has a forvard maximum voltage of 3V (at 60 mA).

![LED Spec Sheet](LEDSpec.jpg)

- I calculated the resistors to be 5 ohms, to prevent the burning, but since we have 10 ohms at the lab and the value I used for calculation is the absolute maximum voltage, it is better to use the 10 ohms
- I wanted to use female headers, but the kind that I wanted to add was not on the list so I added the male version of it with the same drawing.
- I performed the circuit check and it passed.

#### Step 2: Carrying circuit drawing to PCB drawing tool and other things:
- I imported the current drawing.
- I also imported the DXF file below into the user layer to use as a guide for the PCB board design size.

![Camera](Camera_drawing.jpg)

![Step 2](Step2.jpg)

- I drew the PCB shape below from the guide. (Edge.Cuts layer)

![Step 3](Step3.jpg)

- I moved the each component and tried to draw the soldering line connections as neat as possible.

![Step 4](Step4.jpg)

- I performed the check and it passed.
- I wrote RPI Cam HAT on the Silkscreen layer to be engraved.
- Here is the final 3D model of the board I drew.

![Step 5](Step5.jpg)

#### Results
- I think this circuit board is cuttable for next week, but after finalizing the design I noticed the text below online:
- Source: https://www.tomshardware.com/reviews/raspberry-pi-gpio-pinout,6122.html

![Max draw](Max_draw.jpg)

- Probably it is not possible to charge all the 3 LED's from the GPIO port alone, so I might need to also hookup the 5V or 3.3V separate pin here as a third connection.

#### Multimeter and oscilloscope
    - I totally forgot about these, so I'll check them later.

### Trial II

#### Introduction and Schematic editor - KiCad
I did improvement on the design after discussing with Kris that the IO port might not supply enough current to the LEDs.

I added:
- a 5V supply (coming from one of the RPi 5V pins)
- a 30V, 1.7 A, N-Mosfet (needed to activate/deactivate the IO pin with supplying the 5V or not, mosfet is for DC, relay is for AC, gate - IO pin, source - ground, drain - 5V and LEDs)
- a 10 kOhm resistor to IO port connection from Mosfet gate (this is a pulldown resistor, needed for preventing electrical fluctuations)
- GPIO pin (as a net class directive label)

I replaced 3 existing resistors for LEDs with:
- a 1 kOhm trimmer resistor (this one's resistance can be adjusted after soldering with a screw, useful for dimming LEDs without flicker)

I replaced the 2 hole pin to a three hole one (GPIO, ground, 5V supply).

Here is the KiCad Schematic Editor of the board:

![KiCad Schematic Editor](I0.jpg)

Here are the calculations I made for the circuits:
- I first started calculating the minimum resistance (11.1 Ohm) needed to get the maximum brightness using the maximum forward voltage that was given in the LED datasheet.

![Circuit drawing 1](I01.jpg)

- Secondly, I used the the reported absolute maximum forward current of the LED's to see what would burn them out. It turns out, you cannot really supply enough current with 5V from Rpi, you would need 7.5 V to be able to burn the LEDs.

![Circuit drawing 2](I02.jpg)

- Third, I calculated what scale of resistance that I would need, if I have 10% of the 60 mA povided in the datasheet along 3V maximum forward voltage. It seems to be 261.1 Ohm.

![Circuit drawing 3](I03.jpg)

- Since FABLAB has existing 1 kOhm (1000 Ohm) trimmers and since it is only 5 times the 10%, I decided to use these. It means that I will be able to adjust the current between 100% - 2.7% (for 1000 Ohm).

![Circuit drawing 4](I04.jpg)

Tip: DO NOT FFORGET TO CHECK FOR ERRORS!

#### PCB editor - KiCad
In the PCB editor I imported the schematic described above.

Moved the pieces around (R to rotate them), to find out the best layout. 

I slightly increased the size of the board to fit some of the components, compared the first trial and the camera board.

Here are the layers of the board, shown as separate layers:
- F.Cu Layer: Front Copper Layer
    - I also added a text to be engraved, in our machine we need to put it to copper layer, for board manufacturing house we would need to put the text into the silkscreen layer.
    - Here you can connect through resistors (pass through the space in between two parts of the resistor)

![F.Cu Layer](I1.jpg)

- B.Cu Layer: Back Copper Layer

![B.Cu Layer](I2.jpg)

- F. Adhesive Layer: Front Adhesive Layer

![F. Adhesive Layer](I3.jpg)

- B. Adhesive Layer: Back Adhesive Layer

![B. Adhesive Layer](I4.jpg)

- F. Paste Layer: Front Paste Layer

![F. Paste Layer](I5.jpg)

- B. Paste Layer: Back Paste Layer

![B. Paste Layer](I6.jpg)

- F. Silkscreen Layer: Front Silkscreen Layer
    - Here we have the part labels like R1, RV2, D1, ...

![F. Silkscreen Layer](I7.jpg)

- B. Silkscreen Layer: Back Silkscreen Layer

![B. Silkscreen Layer](I8.jpg)

- F. Mask Layer: Front Mask Layer

![F. Mask Layer](I9.jpg)

- B. Mask Layer: Back Mask Layer

![B. Mask Layer](I10.jpg)

- User Drawing Layer

![User Drawing Layer](I11.jpg)

- Edge Cuts Layer

![Edge Cuts Layer](I12.jpg)

- F. Courtyard Layer: Front Courtyard Layer

![F. Courtyard Layer](I13.jpg)

- B. Courtyard Layer: Back Courtyard Layer

![B. Courtyard Layer](I14.jpg)

- F. Fab Layer: Front Fab Layer
    - Here we have the part names, for example LED_Luminus_1206

![F. Fab Layer](I15.jpg)

Here are the layers of the board, all layers together (excluding F. Fab and User Drawings Layers)
- Front view:

![Front](I16.jpg)

- Back view:

![Back](I17.jpg)

Tip: DO NOT FFORGET TO CHECK FOR ERRORS!

#### 3D viewer - KiCad
After this you can view what your would look in 3D in KiCad 3D viewer.

Here are the 3D views (Note: there is a bug about the header pin layout)
- Front:

![Front 3D](I18.jpg)

- Back:

![Back 3D](I19.jpg)

#### Exporting Gerber (.gbr) files and Gerber viewer - KiCad for PCD manufacturing
This is important to be able to send jobs to PCB manufacture houses or to use CopperCAM for using FABLAB milling possibilities.

- Select plot at the PCB Editor window.
- Select format as Gerber.
- Select a directory.
- Include F.Cu only for FABLAB, include other layers such as silkscreen, masks and adhesives for PCB house.
- Select Edge Cuts for plot on all layers.
- Select nothing for FABLAB or plot footprint values and plot reference designators for PCB house.
- Select use extended X2 gerber format.
- Hit plot.

In addition to F. Cu layer gerber files we need the drill file(s) as well. The drill files function will generate two files as PTH (plated through hole) and NPTH (non-plated through hole). The CopperCAM onöy accepts the PTH.gbr files.

- Hit generate drill files.
- Select same output directory.
- Select same format for drill file, map file (as Gerber X2).
- Select drill origin as absolute.
- Hit generate drill files.

You can use KiCad Gerber viewer to visualize these files.

Here are the visualizations:
- F. Cu file:

![F. Cu Gerber](I20.jpg)

- PTH drill file (includes F.Cu layer):

![PTH drill Gerber](I21.jpg)

#### Exporting VRML (.wrl) file from KiCad (in order to get a 3D mesh file (.stl) that can be read in Fusion 360, FreeCad, etc.)
You can use export VRML file format to generate .wrl file.

These file can be converted to an .stl in Blender or any other ofrmat converting software.

Then this .stl could be used in Fusion 360 or other 3D modelling software.

Here is the 3D stl image:

![stl](I22.jpg)

#### Use of multimeter


#### Conclusion
I am quite happy with the secon iteration of the board design after getting help from Kris!
Now the important part is to mill this board and solder it properly.