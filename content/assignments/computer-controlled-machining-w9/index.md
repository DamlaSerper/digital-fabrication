+++
    Title = "Computer Controlled Machining - Week 9"
    weight = 90
+++

## PROCESS

For this week I wanted to make a tissue holder box with two compartments gist to FABLAB. Here is the idealized picture of this:

 ![idealization](idealization.jpg)

What I learned was toolpath for milling is quite similar to the process we used in Roland MX-40 for milling master molds, the same software VCarve Pro is used here as well for this purpose.

I used Fusion 360 to design the tissue holder using the tissue dimensions and the to be milled material thickness (which was 15 mm on paper but turned out to be 14,7 mm).

After designing the box, I learned I need to add dogbones to ensure edge cuts to go and fit smoothly.

For this I used the nifty dogbone add on on Fusion 360, which nicely allows you to select multiple objects to apply the dogbones to and set the tool diameter and tolerance easily.

I used Fusion 360 drawing tool to get .DXF 2D drawing of each part of the tissue box (front, right, back, left, middle and bottom).

Here is the final object in Fusion 360 w. dogbones:

 ![Fusion360](fusion.jpg)

Here are the .DXF 2D images:

Front:

![F](front.jpg)

Right:

![R](right.jpg)

Back:

![B](back.jpg)

Left:

![L](left.jpg)

Middle:

![M](middle.jpg)

Bottom:

![Bo](bottom.jpg)

Later I imported these .DXF files to VCarve Pro, unified the shapes and set the tool related parameters based on the tool and the material thicknesses as I have done in Moulding and Casting week.
Here is the machine website for the maximum values: https://wiki.aalto.fi/display/AF/CNC+Milling+with+Recontech+1312 and https://www.cnc.fi/recontech-1312.html
The same tool library datasheet can be used as the Roland MX-40 (but for soft wood).

(I did not report the exact calculations, since I carried out this design with 3 mm tool in mind and cut it, but the CNC broke during the cutting process. Therefore, the process is repeated by Kris later with 6 mm tool, thus changed values for certain parameters. Also the calculations are the same with Roland MX-40, please see Moulding and Casting Week for these calculations.)

I made outer cuts for the outer parts and inner cuts for the inner parts. Also added tabs for ensuring the parts connection to CNC bed during cutting.

After design is done and the toolpaths are generated in VCarve Pro, we need to import our design to CNC software and physically secure the material to be milled down.

The material was quite bent (cheap soft wood), therefore we needed to use screws with T shape joints to press down the wood as close to the CNC bed as possible.

 ![screw](Screw.jpg)

Since this was not enough to level the surface, we also drilled some material on the the sacrificial layer.

Sacrificial layer (MDF) is used below the material to prevent damage to the machine bed and ensure decent cut depth. The cut depth is selected as 14,9 mm for 14,7 mm material.

After the material is fixed we need to attach the correct milling bit and respective collet on to the machine.

We also need to adjust the milling head to the left bottom corner, keeping the screwed areas in mind.

Here we can set the X and Y axes.

Later we can put the Z level sensor on the surface and lower the tip to get very close to it and then use the Finnish button on the software to get Z level.

After we are done with these, we can go out close the door and press the green button from outside to start the process of milling.

We need to watch the CNC mill to make sure there is no tool breakage or weird high pitched sounds during the process (again like Roland MX-40 but bigger).

Here is the video of the process:

{{< video src="CNC.mp4" type="video/mp4" preload="auto" >}}

After the cutting is complete, we can remove the plywood block, by removing the screws. 

We need to vacuum the room for the leftover dust that is not picked up by the dust collector of the machine.

We need to remove the tools back to their respective containers and move the sacrifical layer back to the scrap boards pile.

After this we can turn off the machine and the software.

The cut object needs to be carried to the woodworking room, where it can be removed from the tabs, attached together, glued if needed (PVA glue).

If glue is used its a good idea to clamp down the sides of the board to ensure perfect connection during glue drying.

Later when the glue is dry the piece can be sanded and if needed one can apply wood oil on it.

Kris performed the gluing and clamping and I did some light sanding with 80 grit sand paper.

Here is the final picture of the object with the clamps:

![Final](Final_CNC_side.jpg)

## CONCLUSIONS

I quite liked the process of designing products to be milled either with Roland MS-40 or Recontech 1312 CNC.