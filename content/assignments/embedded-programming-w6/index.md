+++
    Title = "Embedded Programming - Week 6"
    weight = 60
+++

This week I am doing something different than my Digital Fabrication course peers. While they are focusing on arduino style Xiao board, I will be focusing on Raspberry Pi4, installation of Lite OS, camera imaging using python and service (systemd) files. This is because my final project will involve use of Raspberry Pi v3 camera to capture videos of a rotating centrifuge basket's inner contents.

#### Set-up of a operating system on Rapberry Pi 4
- Start by getting a very large capacity microSD card (mine is 400 GB). 
- Connect this microSD card to you computer using a card reader. 
- Download the RaspberryPi Imager software for your specific operating program.
- After download run this and select the options for the Raspberry Pi operation system. The options I have already set here are:
    - OS: Rapsberry Pi OS Lite (32-bit)
    - Wireless set-up (SSID, password, and network location)
    - User ID and password
    - Language, time, location, keyboard language
- After selecting the above options select the correct disk from the list (your microSD) and download the files of OS
- When the prompt says you can remove the disk, remove it and insert it to the designated slot of Raspberry Pi 4.
- ![Designated microSD card slot on RPi4](microSDslot.jpg)
- Connect the RPi4 to a screen and a keyboard.
- ![Raspberry Pi 4 connected](RPi4.jpg)
- Connect the power cable to the RPi4, which will start the booting process and install the OS on the RPi4.
- Enter your user name and your password.
- Download and install Python3 if not already on! Picamera library should be automatically included with new OS's.

#### How to set up the internet connection and modifying other settings after installing the OS
- For this you can use th command: `sudo raspi-config`
- Which shows you the options below:
- ![raspi-config options](raspi-config_options.jpg)
- Here selecting system options gives us the options below:
- ![system options](system_options.jpg)
- From this menu we can set WIFI settings and etc.
- If we instead selected interface options, we get the menu below:
- ![interface options](interface_options.jpg)
- Here we can enable and disable interfaces such as SSH, camera  and etc.
    - Tip 1: Wireless password of Fablab: Fabricationlab1
    - Tip 2: Wireless SSID of home network: ARRIS-CE22-5G-Damla-Touko

#### Using RPi4 headless with SSH connection to a device on the same Wireless Network
- Start with enabling SSH from the raspi-config, interface options, if not already enabled.
- Have a computer on the same network with RPi4.
- Use `hostname -I` command to get the IP address of the RPi4.
- Use this IP address to connect remotely to RPi4 from your computer using the command in your computer terminal: ` ssh serperd1@193.167.5.168`.
    - Tip 3: The IP address can change every now and then. Here serperd1 is the user name of the RPi4 and the 193.... is the IP address of the RPi4. The IP address definetely changes when you change the wireless network.
- In order to kill the connection simply type `exit`.

#### Connecting and getting a test image with RPi v3 wide angle camera
- Disconnect the RPi4 from electricity, after using `shutdown now` command.
- Open the case and locate the camera cable port.
- Lift the port platic clips, insert the cable so that the connection points (silver lines on the cable) line up with the similarly marked part in the port.
- ![camera connection to RPi4](connect_camera.jpg)
- Lower the clips to fix the cable to the location.
- The other side of the cable should be attached to the camera module itself.
- ![Camera cable connection to the camera module](RpiCamera_v3_WA.jpg)
- After connections are complete, supply RPi4 with electricity.
- When opening is complete use the command: `libcamera-still -o test.jpg` to get the preview and save a test image.

#### Programming the RPi camera
- A better way to use and programme RPi cam is to use Picamera2 library and python scripting.
- You can access the documentation of Picamera2 library from: https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf
- We can write a basic python script with the commands below (camera_preview.py) that autofocuses the camera continuously, takes a preview that alomst covers the whole screen, waits for 30 seconds before taking an image and saving it in .jpg format:

```
import time
from picamera2 import Picamera2, Preview
from libcamera import controls, Transform

picam = Picamera2()

config = picam.create_preview_configuration()
picam.configure(config)

picam.start_preview(Preview.DRM, x=10, y=-100, width=1250, height=1250, transform=Transform())
picam.start()
picam.set_controls({"AfMode": controls.AfModeEnum.Continuous})

time.sleep(30)

picam.capture_file("test.jpg")
picam.close()

```
- Here: 
    - `import time` stands for imprting the time class
    - `from picamera2 import Picamera2, Preview` stands for importing the Picamera2 and Preview classes from picamera2 library
    - `from libcamera import controls, Trnasform` stands for importing the controls and Transform classes from libcamera library. controls is needed if we would like to use the autofocus functionality of RPi cam v3. Tranform class is needed in order to modify the camera preview window.
    - `picam.start_preview(Preview.DRM, x=100, y=200,---)`: we need to use DRM if we are on Lite OS, here the x y are kind of the offsets, width and height are the dimensions of the preview window, transform can be set to different  versions of the Transform function to get views that are mirrored, flipped and etc., but here I just set it to default (without any input parameters) to just be able to use the prior commands (x,y, width,height) for the set-up of the preview window.
    - `picam.set_controls(---)` here we set the autofocus mode of the Picamera to Continuous, we can find more modes in the documentation of the PiCamera2 library.
    - `time.sleep(30)` allows us to make picamera stop doing anything for 30 seconds, in this time since the preview was already on, we will see the preview window.
    - `picam.capture_file("name.jpg")` functionality allows us to capture the image and save it a .jpg file, for other formats check the library documentation.

The screenshot of the preview screen and the final shot saved is given below:

![Preview screen](preview.jpg)

![Test image](test.jpg)


#### Use of fbi to visualize images on RPI OS Lite
- It is not possible to use GUI in Lite version of the Raspberry Pi OS, therefore we need a special package to be able to view the images taken.
- This package is called frame buffer image viewer (FBI) and can be downloaded with the command: `sudo apt-get install fbi`
- To use it just write `fbi name_file.jpg` to the terminal and the image will be viewed this way.
- There also are a lot of options to this function, therefore if needed check documentation: https://manpages.ubuntu.com/manpages/xenial/man1/fbi.1.html

#### Copying files between computers
- One can copy files from the RPi4 to the personal computer with the help of SSH connection again: `scp serper@IPofRPI:/home/username/file_folder/file_name ./`
    - Tip 4: The `./` stands for this directory, which is the current directory in your personal computer.
- The reverse can also be performed: `scp file_name serper@IPofRPI:/home/username/file_folder/`

#### Use of systemd script (service file) to operate the camera automatically on reboot
- One can write a systemd script (a pycamera.service file) to autostart RPi camera upon reboot. 
- For this start by creating a folder in your RPi home directory called systemd.
- Go inside this folder and create a file called pycamera.service.
- Here is a simple service file starts camera preview 5 second after rebooting. For further options check documentation: https://www.freedesktop.org/software/systemd/man/systemd.service.html

```
[Unit]
Description=Pi Camera 5s Late Preview 

[Service]
ExecStartPre=sleep 60
ExecStart=/usr/bin/python /home/serperd1/camera_preview.py

[Install]
WantedBy=multi-user.target
```

- Here:
    - Unit is where we specify the description of the service provided.
    - Service is where we define what the actual actions of service are.
    - ExecStartPre and ExecStartPost are executed before and after the main ExecStart action. If there are multiples they are initiated sequantially.
    - sleep 5 here signifies 60 seconds of sleep, if not unit given it is automatically second
    - ExecStart here takes the location of the python and the location of the python file to be initiated upon reboot.
        - Tip 5: To find the location of the python, write to the terminal `which python`.
    - WantedBy multiuser allows everyuser to get the same behaviour.

- After saving this file copy this file to where the systemd files are stored using: `sudo cp pycamera.service /etc/systemd/system/`
    - Tip 6: Do not forget to add the lass slash in the system/, otherwise you will rename the directory.
- Then continue with the terminal commands below:

```
sudo systemctl enable pycamera.service
sudo systemctl start pycamera.service
sudo systemctl stop pycamera.service
sudo systemctl disable pycamera.service
```

- Here:
    - enable installs the pycamera service
    - start starts the service right now (without waiting for the reboot)
    - stop stops the service right now (without waiting for completion)
    - disable uninstalls the pycamera sevice
        - Tip 7: Always uninstall the service before modifying and do the modifications in your home directory first.

- Here is a video of the service working:

{{< video src="service.mp4" type="video/mp4" preload="auto" >}}

#### Setting up the GitLab connection for the RPi4
- Here are the README.md file contents below and here is the link to the repository: https://gitlab.com/DamlaSerper/rpi4

##### Start of README.md file
*Raspberry Pi 4 camera control*

- There are three files that are kept in sync with the RPi4 here. The first one is camera_preview.py, second on is pycamera.service and the third one is this README.md file you are reading.
- I followed the steps below in order to create the git control structure:
	- I first created a repository in my GitLab account.
	- Then I created sskey pairs on my RPi4:
	- `ssh-keygen`
	- Then I used the cat command to view my key and copy it:
	- `cat ~/.ssh/id_rsa.pub`
	- I saved this SSH key in my GitLab account
		- Click on your profile picture icon
		- Select preferences
		- Go to SSH keys
		- Save the key here
	- Then I created a directory within my home directory to move the files to be kept in sync:
	- `mkdir python_and_service_files`
	- Then I cloned the directory within here:
	- `git clone git@gitlab.com:DamlaSerper/rpi4.git`
	- Then I copied my files camera_preview.py and pycamera.service into here:
	- `cp /home/serperd1/camera_preview.py /home/serperd1/python_and_service_files/rpi4/`
	- `cp /home/serperd1/pycamera.service /home/serperd1/python_and_service_files/rpi4/ ` 
	- Then I checked git status:
	- `git status`
	- Then I added the two files to shelving area with:
	- `git add *`
	- Then I configured my email and user name:
	- `git config --global user.email damla.serper@aalto.fi`
	- `git config --global user.name Damla Serper`
	- Then I committed the changes:
	- `git commit -m "added the python and service file"`
	- Then I pushed the changes to GitLab:
	- `git push`
	- After these I also modified the README.file briefly with nano and did the above steps to sync.
	- I also set-up the same folder in my Linux computer and set the SSH connection to be able to efit the files peacefully in Visual Studio Code. 
- To use the files: 
	- First file sets the preview behaviour and image taken (size, how long, what image file type to use)
	- The second file sets when this python code will be eexcuted, with respect to rebooting time.

##### End README.md file

- Here is an image showing set git connection on my Linux computer:

![Linux git folder](Linux_git_ssh.jpg)

#### Conclusion

I learned a lot using the RPi Lite OS. I was actually quite scared to begin with due to the lack of GNU. Although after getting used to it, it seems to be quite easy to handle. I will expand on this camera control in the upcoming weeks for my final project especially after also building my RPi-LED-HAT. A button could be also nice to initiate the LED lights shining, focusing the camera once at the beginning of the operation, and starting the camera capture with wanted controls (exposure rate is very important in rotating systems to get a still image).