+++
    Title = "Input - Output Devices - Week 11 and 13"
    weight = 110
+++
## PROCESS
For these two weeks I designed a board Kicad, where I hooked up an OLED display (output) and three buttons (input).

Used CopperCam to generate the toolpaths (where I increased the line thinkess to 0.5 mm), Roland MX-20 to mill the board, lead free solder to solder the parts and multimeter to check the connectivity.

The idea was to use this board in the final project to show preview of camera recording, and also 2 of the buttong to wirelessly control autofocus of the camera and LED's of the LED-hat that I have built in the electronics production week.

For weeks 11-13, I wanted to just test out a code to make LED screen work with push of the button.

Arduino part of this was completely new for me since I have done the prior board to fit and be coded with Rpi.

The design in KiCad schematic editor looked like this:

![schematiceditor](schematiceditor.jpg)

 I used 14 male headers (7 x 2) with 2.54 mm pitch in order to connect Xiao-ESP32-C3 board to the PCB board that I designed (let's call it camera control board).

 Xiao board pin diagram:

 ![pindiagramXiao](pindiagramXiao.jpg)

 To attach to these male headers, I soldered 2 times 3 and 4 lane vertical connection pin sockets with 2.54 mm pitch on the camera control board. 

 Using these I managed to get surface mounting instead of through hole soldering which was easier to execute.

 For the OLED screen, I have used AzDelivery 0,96 inch OLED screen (GME12864-11), which turns out to be a knock-off version of Adafruit SSD1306 OLED displays. This screen had four connections as ground, power supply (recommended 3.3V), SCL and SDA special IO ports. Here is the website to this part where you can find its documentation as well: https://www.az-delivery.de/en/products/0-96zolldisplay. Also a link to the adafruit library on github: https://github.com/adafruit/Adafruit_SSD1306 ,https://github.com/adafruit/Adafruit-GFX-Library and examples on how to use the libraries: https://learn.adafruit.com/monochrome-oled-breakouts/arduino-library-and-examples.

 Therefore I connected the Ground (1) to pin number 13, 3,3 V supply (2) to pin number 12, SCL (3) to difital port 5 (pin number 6) and SDA (4) to digital port 4 (pin number 5). 

 I attached a button between Xiao 3,3V supply and the OLED (button SW3, BUTTON_OLED).

 I attached two more buttons to digital ports 0 and (pin numbers 1 and 2), these are not used in Week 11-13's coding session.

 Here is how I layout looked like in KiCad PCB editor:

  ![PCBeditor](PCBeditor.jpg)

 And how it looked in KiCad 3D viewer:

  ![3Dview](3Dview.jpg)

 Finally how it looked like after soldering in real life:

 ![soldered](soldered.jpg)

 I wrote the code below to test out the functionality controlling the OLED with button pressing, for this I used couple of example cases and joined them to make sense.

 ```
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define BUTTON_OLED 10  
#define WIRE Wire

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &WIRE);

void setup() {
  Serial.begin(9600);

  Serial.println("OLED button test");
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32

  Serial.println("OLED begun");

  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen
  // internally, this will display the splashscreen.
  display.display();
  delay(1000);

  // Clear the buffer.
  display.clearDisplay();
  display.display();

  Serial.println("IO test");

  pinMode(BUTTON_OLED, INPUT_PULLUP);

  // text display tests
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0,0);
  display.print("Connecting to SSID\n'adafruit':");
  display.print("connected!");
  display.setCursor(0,0);
  display.display(); // actually display all of the above
}

void loop() {

  if(!digitalRead(BUTTON_OLED)) display.print("BUTTON FOR OLED WORKS");
  delay(10);
  yield();
  display.display();

}
```

After this I menage to make screen light up with continuous press of the button, although I could not pass the splash screen of Adafruit to write something on the screen.

Here is a video:

{{< video src="splash.mp4" type="video/mp4" preload="auto" >}}

## CONCLUSIONS

I am more satisfied with my second board compared to LED hat from the electronics production week. I think I got better at soldering (only 2 boards went to scrap :D, as opposed to 10). 

I did not want to add sensors because I already did quite complicated design for the initial LED hat and besically made boards for both Arduino and RPi environment.

It took me sometime to figure out what to do with Arduino IDE, but managed to make the screen mostly work. 

I would like to learn more about the Arduino library to be able to use this OLED screen as camera preview screen for the final project.