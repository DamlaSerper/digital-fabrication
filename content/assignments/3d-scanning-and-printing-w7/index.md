+++
    Title = "3D Scanning and Printing - Week 7"
    weight = 70
+++

### Introduction
- This week I wanted to experiment with printinga pipe with 120 degree internal separator and thread outside to be attached to somewhere with a nut. 
- I wanted to experiments with printing this in FDM printers without support aligning them in different orientations in the slicer (vertical and horizontal).
- I also wanted two try two of the design rule tests to understand a bit more about the unsupported prints, the angles and overhang of those.

### Design rule tests
#### Conditions
- I tried printing two of the design rule tests: unsupported angle and unsupported overhang, using Ultimaker S3 with:
    - 15% infill,
    - triangular infill,
    - 0.2 mm layer height,
    - 70 mm/s print speed, and
    - brim type build plate adhesion.
- I used the already installed yellow PLA 0.4 mm, while disabling the support material print with loaded PVA material. 
#### Results
##### Unsupported angle:

![Unsupported angle](angle.jpg)

- As you can see here with the reduced angle (or increased bend angle), one gets bad printing performance without the insertion of the supports.
- With the current settings, we can say the print starts failing significantly under 40 degree angle. 

##### Unsupported overhang:

![Unsupported overhang](overhang.jpg)

- As you can see here with the increased overhang length, one gets bas printing performance without the insertion of the supports.
- With the current settings, the print starts failing already as short as 2 mm, anything above that is not recommended to print without supports.

### Pipe with 120 degree separator and threading

#### Design in Fusion 360

- I drew all the elements of the centrifuge in Fusion 360. 
- After investigating the positioning of the old pipe, I decided to draw a thinner new pipe as below:
- I used parameteric design, although did not give the parameters used here, those will be documented on the final product page along the rest of the centrifuge geometry drawn.

##### Old pipe

- Front view with outer basket lid, inner basket lid and inner basket:

![Final old front](final_old_front.jpg)

- Front-top view with inner basket lid and inner basket:

![Final old top front](final_old_top_front.jpg)

##### New thinner pipe

- Front view with outer basket lid, inner basket lid and inner basket:

![Final new front](final_new_front.jpg)

- Front-top view with inner basket lid and inner basket:

![Final new top front](final_new_top_front.jpg)

##### Steps of design
- Here are the steps of the Fusion 360 design:
    
    - Draw a 16 mm diameter circle.

    - ![Step 1](step1.jpg)
    
    - Draw a 14 mm diameter concentric cicle, within the first one (width of the new pipe = 1 mm).
    
    - ![Step 2](step2.jpg)
    
    - Draw a line from center to inner wall at 0 degree and make it a construction line.
    
    - ![Step 3](step3.jpg)
    
    - Use circular pattern tool, select the construction line as the pattern to be copied, origin as the axis and the number of copies to 3. This will create 120 degree apart construction lines.
    
    - ![Step 4](step4.jpg)
    
    - Draw the complementary length of one of the contruction lines.
    
    - ![Step 5](step5.jpg)
    
    - Use the circular pattern tool again with the same settings except the pattern to be copied being selected as the complementary line. Also create construction lines that are 0.5 mm further away from the center on the two construction lines that are in the either side of 0 degree construction line. These will be used as guides for the divider.
    
    - ![Step 6](step6.jpg)
    
    - Use the 0.5 mm points to draw two parallel lines, that ends at the inner wall of the pipe.
    
    - ![Step 7](step7.jpg)
    
    - Use the circular pattern tool again but this time to copy the two lines that you have created.
    
    - ![Step 8](step8.jpg)
    
    - Now turn the inner circle into a construction line, and use the three point arc tool to draw one of the arcs 
    that are split by the divider. Finally use the circular pattern tool once more to copy these arcs.
    
    - ![Step 9](step9.jpg)
    
    - Select the surface in 2D and use it to extrude with an offset (to get the pipe in the correct location) in both directions (aiming for the same height with the old pipe).
    
    - ![Step 10](step10.jpg) 
    
    - Use the thread tool to create a thread on the outer surface of the pipe in the designated area with the settings below:
    
    - ![Step 11](step11.jpg)
    
    - To get the final image:
    
    - ![Step 12](step12.jpg) 

#### Slicer settings
- I wanted to try printing this pipe without support.
- I aligned the pipe in one copy horizontally and in one vertically to see the printing performance.
- I actually printed out all the design tests and the pipe copies in one go.
- Alignment of the tests and pipes:
- ![Brim](brim.jpg)
- The settings I used for printing are the same as the design rule tests, described in the first section.
- Here is a video showing the printg process:
- {{< video src="3Dprint.mp4" type="video/mp4" preload="auto" >}}

#### Print results
##### Horizontal pipe

|![H1](H1.jpg)|![H2](H2.jpg)|![H3](H3.jpg)|
|---|---|---|

##### Vertical pipe

|![V1](V1.jpg)|![V2](V2.jpg)|![V3](V3.jpg)|
|---|---|---|

##### Discussion of results

As one can see from the above images, the vertical orientation of such pipe is prefereble over horizontal orientation. When oriented horizontally we see failiure in the curvature of of the pipe and the thread structure. The vertical orientation on the otherhand produced an almost usable design (at least a functioning thread and almost uniform print quality across the length). Although with print height increasing, the structure starts sahking a lot more and even though brim was able to keep the structure attached to the bed, the final layers at the top became lose and inaccurate (this can be seen in the final image). Therefore I would not recommend making a taller print this way. Also the texture of the print unfortunately would affect the flow pattern in such pipe, therefore I would recommend acotually using SLA for pipe printing. I am aware I used a very coarse layer height and fast print speed, although the FDM is still limited to way larger layer heights compared to SLA, so I would still recommend using SLA for pipe printing.
 