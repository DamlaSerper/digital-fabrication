+++
    Title = "Molding and Casting - Week 14"
    weight = 140
+++

### Introduction
I wanted to design a small plastic cat dish as a decorative item and cast it from a liquid plastic within a silicone mould.

Since the mould material I would like to use is liquid silicone, cating of this requires a master mould that is carved out of solid wax block.

For this I followed the steps below:

#### Design of the object and the moulds

For this, we need to design an object, a mould around the object and the master mould of that mould.

Here is the design of the master mould designed in Fusion 360:

![Master_mould_design](0.jpg)

The above master mould design is exported in .stl format.

#### Creation of toolpath in VCarve Pro 9.5

In order to mill/cut master mould design out of  a solid block, we need to first create toolpaths (instructions) for Rolan MX-40 machine to read.

This can be done by importing our design into VCarve Pro 9.5 software.

It is a good idea to try cutting out the design from foam first in order to make sure the design we have in mind actually works and cuts as expected.

This is a good idea because the foam is a cheaper and a softer material. Softness help because the milling is faster on a softer material, so we can spot our mistakes earlier.

After foam cut successfully works, we can continue to cutting on hard wax block.

If the foam or wax surface is uneven, it is smart to even these out using the 22 mm butterfly milling bit.

This bit is used for cutting out big chunks of material fastly.

##### 1. Evening out material surface

In my case the foam block I found in the room was quite uneven on both top and bottom surfaces, therefore I started by evening out the surfaces of this foam block.

When I initially measured the dimensions of the block to be used, I took the heighest points of each dimension. The depth of irregularities was 20 mm at the top and 4 mm at the bottom.

The dimensions are given below:

|**Width**|**Height**|**Thickness**|
---|---|---
|**X-axis**|**Y-axis**|**Z-axis**|
|130 mm|115 mm|45 mm|

When we first open the VCarve Pro software we need to open a new file.

Select single sided job and enter the job size as the above table values.

Select Z zero position as the material surface.

Select XY Datum position as left bottom corner without any offset and finall modelling resolution as standard.

Then we hit ok!

![Toolpath](6.jpg)

We need to draw rectangle as big as the block and hit cancel.

After this select the pocket path tool in order to create toolpath for surface evening out.

![Master_mould_design](2.jpg)

Select the cut depth as 20 mm (5 mm when evening out the bottom of the foam, also remember to adjust dimensions accordingly).

Click on the select on tool section.

Adjusting the tool parameters according to guidance below:

- Select spindle speed close to maximum spindle speed of the machine. Since the maximum spindle speed is 15000 rpm for Roland MX-40, we are using the value 14000 rpm. (Machine maximum spindle speed can be found on: https://www.ati.com.ph/products/roland.web/products1/mdx40a/mdx40a.specs.htm)

- Calculate feed rate from `Feed Rate = Number of flutes * Chip load * Spindle speed` and compare it to the maximum value of the feed rate. If this value is above the maximum feed rate of the milling machine, use a value close to machine maximum. The feed rate calculated from the formula is 5600 mm/min which is above the maximum value for Roland MX-40 (50 mm/s = 3000 mm/min), therefore we will be using 2500 mm/min as the feed rate. (The number of flutes is indicated on the tool bit itself, The chip load could be found on: https://www.dorotec.de/webshop/Datenclaetter/fraeser/schnittwerte_en.pdf, select MDF and stainless steel as reference compounds for foam and wax respectively, check the correct diameter of the tool bit in the chip load chart, the Roland MX-40 maximum feed rate can be found at: https://www.ati.com.ph/products/roland.web/products1/mdx40a/mdx40a.specs.htm)

- Calculate plunge rate as 1/4th of the feed rate, so in this case it will be 625 mm/min.

- Select pass depth as 2 mm for foam in case of evening surface out.

- Select stepover as 40%.

![Master_mould_design](1.jpg) 

After accepting the tool settings continue to the pocket section parameters.

Select raster, climb and pocket allowance as half of the tool diameter, and negative. So in this case this value will be -11.

Give it a name and press calculate to generate toolpaths.

##### 2. Importing .stl

Select import .stl file after setting up dimensions and drawing the rectangle.

![Master_mould_design](3.jpg)

Set the settings like above.

Make sure your model is aligned within the rectangle you've drawn as shown below:

![Master_mould_design](5.jpg) 

Hit ok!

Here is the model imported within the rectangle work area.

![Master_mould_design](4.jpg) 

##### 3. Roughing

2 flute 6 mm flat end tool is used for roughing the material. 

Roughing does generate the general shape of the design but it does not create a smooth detailed finish.

Follow the same steps in pocket section, unless described otherwise. 

I selected Z level raster X in both (wax and foam) because I liked the pattern of generating the toolpath better.

After setting up the roughing settings, name the job and hit generate to get the toolpaths.

###### 3.1. Roughing on foam

I set the dimensions to surface evened values below:

|**Width**|**Height**|**Thickness**|
---|---|---
|**X-axis**|**Y-axis**|**Z-axis**|
|115 mm|104 mm|25 mm|

Cut depth for the tool is selected as 2 mm since foam is a soft material.

![Roughing_on_foam](7.jpg)

###### 3.2. Roughing on wax

I set the dimensions to values below:

|**Width**|**Height**|**Thickness**|
---|---|---
|**X-axis**|**Y-axis**|**Z-axis**|
|86 mm|79 mm|37 mm|

Curdepth for the tool is selected as 1 mm since wax is a hard material.

![Roughing_on_wax](1_w.jpg)

![Roughing_on_wax](2_w.jpg)

##### 4. Finishing 

2 flute 3 mm ball end tool is used for finishing the material.

Finishing comes after roughing is done, you do not need to reimport the model or reset the dimensions.

There are other options as flat end 3 mm tool and even 1 mm tools, but I decided to go with the above option for finishing the surface detail.

Ideally using multiple finishing and roughing steps would help getting a nicer more detailed finish although due to length of milling on hard wax, I have limited to steps to one for each. 

I selected raster in both (wax and foam) because I liked the pattern of generating the toolpath better.

After setting up the finishing settings, name the job and hit generate to get the toolpaths.

###### 4.1. Finishing on foam

![Finishing_on_foam](9.jpg)

Selected stepover as 0.5 mm (The smaller the value the finer the detail and the longer the duration of milling).

###### 4.2. Finishing on wax

![Finishing_on_wax](4_w.jpg)

![Roughing_on_wax](3_w.jpg)

Selected stepover as 0.25 mm because wax is our actual master mold and nicer finish is desired.

#### Use of VPanel and Roland MX-40 milling machine for the master mould

After generating all the toolpaths, one would first insert the tool for evening out (if there is any need), then roughing and finally finishing.

Using Vpanel one can move around the base plate of the milling machine, exactly like in Roland MX-20.

So the idea would be start with selecting correct cullet for the tool bit (22mm butterfly and 6 mm tool bits use 6 mm cullet, while 3 mm tool bit uses 3 mm cullet).

Screw in the collet, use the wrenches adjust the tool bit in.

Use the Z level sensor (gold side up) and select detect, the machine will automatically lower the tool until it slowly touches the Z sensor. Other way of doing this is using a paper to adjust the zero Z.

After this bring the tip to where southeast corner of the cut will be (tip center right at the southeast corner) and set X, Y coordinate.

Set the speed cutting speed 50% for foam and 20% for wax to start with.

Set the spindle speed 100%.

Select setup -> command set -> NC-Code

Select G54 coordinate system

Select cut and add your respective toolpath file.

Press cut.

If you see the cut is going well you can increase the speed as long as you do not hear a high pitched voice.

I managed to increase speed up to 150% for wax.

Example control panel:

![VPanel](7_w.jpg)

Here is the cutting process for foam:

{{< video src="vid1.mp4" type="video/mp4" preload="auto" >}}

Here is the cutting process for wax:

{{< video src="vid2.mp4" type="video/mp4" preload="auto" >}}

Before and after of evened out foam:

![Beforesurfaceevening](a.jpg)

![Aftersurfaceevening](d.jpg)

Here is the final milled master mould from foam:

![Milledmastermouldfoam](b.jpg)

Here is the final milled master mould from wax:

![Milledmastermouldmax](f.jpg)

#### Silicone casting of the mould

I have used Mold Star™ 15 slow, which is a platinum silicon rubber in a visous liquid form (https://www.smooth-on.com/products/mold-star-15-slow/).

It is formed by reaction of two parts with equal volume or weight, color of the mixed liquid silicone turn greenish blue. 

![BottlesMoldStar](l.jpg)

This specific silicone has a pot life of 50 minutes and cures in 4 hours at room temperature.

You must use vinyl gloves only with this product since latex inhibits the curing of the silicone.

Another inhibitant of cure is sulfur, therefore one must be aware of the composition of their master mould.

Using in a well ventilated area is recommended.

Both of the pots needs to be mixed with different sticks making sure the separate parts are not settled down.

Then equal parts should be dispensed into a plastic cup (to determine the approximate volume use water, fill the master mould, add some additional volume for flexibility in poruing, divide the volume in half and more on a plastic cup and finally dry the mould and cup before pouring parts A and B or mixed silicone in) with a thin stream of pour to reduce air bubbles.

One needs to completely incorporate to parts by mixing (use a different stick), color difference helps here.

To prevent bubbles in the final mould, I degassed (vacuumed) my silicone mixture before and after pouring in the master mould couple of times.

Degassing vacuum pump:

![Degas](i.jpg)

After mixing pour in a thin stream to avoid bubbles.

Poured silicone in master mould: 

![Pouredsilicone](h.jpg)

Cured silicone in master mould: 

![Curedsilicone](q.jpg)

I did not need to use mould release, since mold start silicone is quite flexible. 

I was able to use a small knife to push sides of the cured silicone to release it from wax master mould.

#### Liquid plastic casting of the cat dish

I have used Smooth-Cast™ 305 medium setting white which is a urathene plastic (resin) in liquid low-viscosity form (https://www.smooth-on.com/products/smooth-cast-305/).

It is formed by reaction of two parts with equal volume (or 100 A:90 B by weight), color of the mixed and individual parts look grayish clear, while the cured product is white opaque. 

This specific resin has cure time of 30-40 minutes, although post curing for 4-6 hours recommended.

Using in an extremely well ventilated area required, thus I used the ATEX room to do this mixing.

So the difference from the silicone casting, I marked the volumes beforehand.

Afterwards I went to the ATEX room with air suction and mixed and poured my liquid plastic there. 

I left the plastic to cure there, without using degassing (due to unavailability of the electric sockets).

I did not need to use mould release, since mold start silicone is quite flexible and it is stated as mostly unnecessary in the documentation for coupling liquid plastic with the mold star.

Poured liquid plastic in silicone mould:

![Pouredplastic](o.jpg)

Cured plastic in silicone mould:

![Curedplastic](n4.jpg)

![Curedplastic](n3.jpg)

Side by side demoulded master and normal mould:

![Sidebysidemoulds](s.jpg)

Needed to sand the backside of the cured plastic due to shrinkage with 80 grit sandpaper.

![Curedplasticsanded](n2.jpg)

### Conclusions

I learned a lot of things about using Roland MX-40, generating toolpaths, designing moulds and in general chemical reaction of silicone and resin casting. 

I am impressed with how good the Roland MX-40 is cutting foam, therefore I am thinking of using MX-40 in the future for preparing miniature topology creation.

Silicone casting was very fun and I can see the hype in resin, silicone casting that is going on on Youtube.

I believe my cast was mostly sucessful, my only comment would be not to have too small features (like the cat eye and nose detail in my original design), because you might not achieve these with even the 1 mm tool bit, and there is a risk of damaging the tinier tools with hard wax block.