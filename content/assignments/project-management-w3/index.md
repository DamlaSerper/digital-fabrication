+++
    Title = "Project Management - Week 3"
    weight = 30
+++

## Week 1-3
Here I will describe the process of designing my website from week one to three. When I started I had no prior experience in web-design except some class that I took on html approximately nine years ago. I was already experienced in using Linux computer and terminal by the time I started this course.

### 1. Website creation locally with HTML
- Local website generation is carries out with plain html commands that is written in an .html file. 
- I have used visual studio code to edit the contents of this html file.
- It is possible to open these files on a browser locally.
- The general idea here was to create some pages: index.html, about.html, final-project.html, sandbox.html and notebook.
||1|2|3|4|5|
|--|:--|:--|:--|:--|:--|
|file name|index.html|about.html|final-project.html|sandbox.html|notebook.html|
|page name|Main Page|About Me|Final Project|Sandbox for HTML|Notebook|
|explanation|The first page that comes up when you click the website URL|The place where I introduce myself and why I am taking this course|The place for documenting the Final project - there is just a temporary cat picture|Sandbox is the first page I created where I was playong with basic HTML commands, I wanted to have this page to explore HTML commands when needed|Notebook is where I record the new commands I learn for example Git and GITLAB related commands|

#### First Page (index.html)
```
<!doctype html>
<html>
    <head>
        <title>FABLAB - DIGITAL FABRICATION 1- Course Assignment Portfolio</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <nav>
            <table width="25%">
                <tr>
                    <td>
                        <a href="about.html">
                        About</a>
                    </td>
                </tr>
            </table>
        </nav>
        <header>
            <h1>PORTFOLIO</h1>    
        </header>
        <section>
            <p>This website will be used for documenting progress during FABLAB - Digital Fabrication 1 Course</p>
        </section>
        <footer>
            <p>&copy; Damla Serper 2023</p>
        </footer>
    </body>
</html>
```

Explaining the code line by line:
- Specifiying it is a HTML style document
- HTML opening tag
- Here we put some invisible things to adjust how the webpage behaves
- Title of the webpage is set
- By using utf-8 character set we can have special characters in the website
- This is how you make your webpage autoset its width 
- As mentioned inside
- Closing tag of head
- Opening tag of body where we right the visible things
- Navigation tag is where we will write the links to other pages
- Will create the navigation links within a table with width of 25% of the webpage
- Opening row tag
- Opening column tag 
- Reference making tag href is where we give the reference
- About is giving the visible name of the clickable link
- Closing column tag
- Closing row tag
- Closing tag for the table
- Closing tag for navigation part
- Header is where we give the header of the specific page
- h1 signifies level 1 header (largest)
- Closing tag for header section
- This is a section opening tag an it is used mostly when  there is more than one paragraphs under the header, one can also use main when there is one
- Paragraph tag and the text
- End of the section tag
- Footer opening tag, this is where one can right copyright information
- Copyright text with special copyright character tag (& copy ;)
- Closing tag of footer section
- Closing tag of the body of the page
- Closing tag of HTML code

Here is how the page looked like:
![Index_page_screenshot](index_page.jpg)

#### Second Page (about.html)
In this page I had the commands below for `<section>` section. The prior and rest of the page was pretty much identical to index page with changes in page titles. The difference in this section is that I introduced two pictures within a table.

```
...
        <section>
            <p> 
            My name is <i>Damla Serper</i> and this is <b>my portfolio</b> website for FABLAB - Digital Fabrication 1 course.<br>
            
            I study chemical process engineering and am currently a doctoral researcher at Aalto CHEM.<br>
            
            I am interested in learning fast prototyping to design imaging system for chemical process machinery and to satisfy my own curiosity.<br> 
            
            Some personal facts: I like watercolor painting, crafting and boardgames. Below you can see me and my cat!<br>
            </p>
            <table border="6" width="50%" cellspacing="10" cellpadding="5">
			    <tr>
				    <td>
					    <img src="human.jpeg" width="100%">
				    </td>
				    <td>
					    <img src="cat.jpeg" width="100%">
				    </td>
			    </tr>
		    </table>
        </section>
```

Explaining the code:
- i tag makes text italic while b tag makes it bold, br tag allows html to go to newline
- Here I set a table with border style 6 (not a plain boring one), width 50% of the webpage, cell spacing of 10 and padding around the content 5.
- img tag is used for adding images, here I set the image width to 100% within its container (in this case it is the table cell) and src is where one gives the file name as a source to this command
- I added the second image to the second cell

Here you can see how the page looked like:

![About_page_screenshot](about_page.jpg "Lovely pictures")

#### Third page (final-project.html)

Here again the only difference was the `<section>` section. I added some filler content for now and my cat's picture as a filler image.

```
...
        <section>
			<p>This is my final project page for the course. Below you can see the final project when I document it :D Until then ...</p>
			<img src="cat.jpeg" width="100%">
        </section>
```

Here is how the page looked like:

![Final-project_page_screenshot](final-project_page.png "Lovely cat")

#### Fourth page (sandbox.html)

I use this page for trial and error and learning plain html. I wanted to keep this page as a sandbox for further experimentation. The different part from the other pages is where I tried the ordered and unordered lists. These lists can be nested within each other, also can be mixed with tables.

```
...
        <ol> Ordered List
			<li>Cat</li>
			<li>Hedgehog</li>
			<li>Duck</li>
		</ol>

		<ul> Unordered List
			<li>Dog</li>
			<li>Seal</li>
			<li>Fox</li>
		</ul>
```

Explanation of the code:
- ol tag stands for ordered list, meaning starting with 1.2.3....
- ul tag stands for an unordered list, where you have markers
- li tag is used for writing the list items

Here is how the page looked like:

![Sandbox_page_screenshot](sandbox_page.jpg "Loyalty free cat")

![Sandbox_page_screenshot_2](sandbox_page_2.jpg "Loyalty free hedgehogs")

#### Fifth final page (notebook.html)

I wanted to create this page for noting down commands that I have learned and will learn for example about Git and GITLAB CI. These are added with the same structure as above and their content will be discussed in the upcoming sections. The only important syntax here is defining the section id as below, which allows us to refer to this section leater:

```
...
        <section id = "Gitcommands">
            <h3> Git commands</h3>
            <p>  git init (to initialize a git directory <br>
            </p>
        </section>
```

Here is how the page looked like with git commands:
![Notebook_page_screenshot](notebook_page.jpg)

### 2. Using git to version control

- After creating the general structure of the website, we made the folder, that contains the html files, a repository by using `git init` command.
- Here the idea is to be able to version control either as single modifier or one of the modifiers of the file.
- We set up our name and email adress using `git config` command to be able to tell apart different modifiers.
-How the logic goes is that:
    - Check if you have unshelved/modified files with `git status`
    - To check the differences before commit/shelving use `git diff`
    - Add the unshelved/modified files with `git add`
    - Commit these changes to the repository and write a message eith `git commit -m ""`
    - Push these changes to a online repository -> Next section

### 3. Generating SSH keys for authentification for GitLab

- SSH keys provide a better protection against hackers compared to just passwords.
- SSH keys are generated in pairs as private and public keys.
- Private key stays at client and public key can be shared freely.
- One should never share their private key since this allows others to decrypt their public key.
- To generate an SSH key you can use the command below:
    `ssh-keygen`
- This promt will ask you where to save the key, the default directory is `~/.ssh` and the keys by default will be called `id_rsa` and `id_rsa.pub` respectively.
- The prompt will also ask you a catchphrase for additional security but you can leave it blank if you wish so.
- You can use `cat ~/.ssh/id_rsa.pub`command to visualize your public key
- After generating this key I copied the public key to the GitLab, for this:
    - Click on your user icon
    - Select preferences
    - Go to SSH keys
    - Paste the copied public key there and select for how long it should be available

### 4. Using GITLAB to publish the website online

- If we have a GITLAB account and create a repository there, we can select the clone option on the GUI and get the git adress of our online repository which will be needed to push changes to this repository form local.
    - Push by `git push online_repository_git_address.com branch`
- After this we have uploaded our website to an online repository, although this only tracks versions.
- We also wanted to be able to publish this website on the GitLab server. For this we can use a file called GitLab CI yml.
- The commands wihtin the basic html GitLab CI yml is given in the Notebook - GitLab CI section. But in order to create this folder we can use the simple GUI of the GitLAB.
- In order to do this we click add, file and select GitLab CI yml with HTML template which produces an automatic template for us.
- We can then use the commit button below to commit this file to our online repository (of course after commenting).
- This creates a pipeline and a processes within that are running, you can see this by clicking on the blue half full circle icon in the main page of the online repository, which takes you to the terminal.
- It gives a green light when it is successfully completed.
- The instructions for this pipeline is given in this GitLab CI yml file we have recently created and committed.
- In order to find our published website on GitLab servers, we can go to Deployments and Pages where we can see a webpage URL.
- By clicking on this we can visualize our website.

### 5. Using GITLAB and git to update the website with local changes

- Any changes we perform on the online repository needs to be pulled before we are able to manipulate the html files further in the local. This is done by the command below:
    - `git pull online_repository_adress.git branch`
- Here the online adress is the one you can find on the GitLab website under clone as close with ssh, which starts with git@gitlab.com...
- But before doing this it would be wiser to add git remote to our local repository to keep our local and online repository as one brach and in sync. 
- This way we will not need to use online_repository_address.com or branch name and can just use commands as `git pull`or `git push`
- This procedure will be done only once when adding the remote
- To add our online directory as remote we can use the command below:
    - `git remote add origin online_repository_adress.git`
- And to see the remote repository we can use the command:
    - `git remote -v`
- After these are done the same workflow in section 2 can be employed to manipulate the files and `git push` command for pushing it back to online repository.
- There is no need to update anything about the server, we will just need to wait for the pipeline to run again automatically and complete to deploy the webites modified version.

### 6. Introduction to HUGO
- Using only HTML or HTML and CSS creates a lot of boiler-plate code that is repeating and long.
- In order to make the work flow more efficient we started using Hugo static website generator, which also works with GitLab server websites.
- HUGO makes use of easier to write markdown(md) files and split these and html files into logical folders as below:

```
HUGO
    config.toml
    content
        _index.md
        subfolders
            _index.md
            subfolder_subfolders
                index.md
    layouts
        index.html
        _default
            single.html
        partials
            head.html
            nav.html
            foot.html
    static
        css
```
Explanation of the folder/file hierarchy
- Main folder
- Configuration file for Hugo websites, we can write here about title, copyright, character sets to be used, base url of the website and even preferences for Table of Contents. Example:
```
baseURL = 'https://damlaserper.gitlab.io/digital-fabrication'
languageCode = 'en-us'
title = 'Digital Fabrication Portfolio'
copyright = 'Damla Serper 2023'
[markup]
    [markup.tableOfContents]
        endlevel = 6 <!-- These are the table of content instructions for which headings to consider for that list-->
        ordered = false <!-- What kind of list that should be ordered or unordered-->
        startlevel = 2
```
- Where we store the content
- Content storing markdown file
- Examples: about/assignments/final-project/sandbox/notebook
- Content storing markdown file
- Examples: project-management-w3
- Content storing markdown file and only the final md file in the directory get index.md name the prior ones get _index.md name
- Where we have the html files that will be integrated with the md content we have written in contents
- Main page
- Where we store our single.html file 
- Where we write the general structure of the html pages of our website except the main page
- Which are repeating parts of an html file and our main html page (index.html)
- Where we write the beginning of the header and beginning of the bod
- Where we write the navigation to each page and etc. (Here we can use the { {.Site.BaseURL} } to get links to each section)
- Where we can store out copyright information and closing tags of body and html
- Where we can have our asset folder (for files not to be converted by Hugo and css files) Example: sandbox.html

- Also don't forget Hugo requires a different style of CI file in GitLab.
- For that reason you can use the GitLab file generator and select GitLab CI .yml and the template as Hugo.
- Just don't commit the file but rather copy the content and paste it to your local CI file.
- Do git commands to commit this update and push it online, pipieline should be automatically updated.

### 7. Markdown files in Hugo
-Markdown files start with a block where you can write unseen stuff like title 
```
+++
     Title = "Final_Project"
+++
```
- There are a lot of variables from Hugo's side that can be called in html files. These variables are marked within `{{}}`.
- These can be used to define content in markdown files.
- You can also put `weight = 10` kind of command to give weight to different titles.
- Example:
 ```
+++
     Title = "Final_Project"
     weight = 10
+++
```
- This helps Hugo to arrange the subsections titles, the heigher weight goes to bottom.
### 8.Using HUGO to make the website workflow easier 

- After introduction of Hugo, I moved my content to Hugo.
- Now I have 5 major sections: About me, Final-Project, Notebook, Sandbox for HTML and Assignments, apart from the main page.
- I have filled Assignments with subsections for each week of FabAcademy.
- I learned how to link **sub-sections**. One can go to nav.html and get subsections automatically with below commands:
```
{{ with .Site.GetPage "/assignments" }}
    {{ range .Pages }}
        <a href="{{ .RelPermalink }}">{{ .Title }}</a>>
    {{end}}
{{end}}
```
- `.Site.GetPage` gets the amount of pages under
- `with` is like if in Hugo, therefore as long as there are pages under that assignment folder this condition will be activated
- `range` is more like a for, here using .Pages tag will allow us to go through those subsections
- `. Permalink` here gets the adress of that subsection like project-management-w3.html
- `. Title` gets the title of the page written in the md file title section

- One elegant way to automatically create **Table of Contents** is to create a `list.html` file wihtin `layouts/_default`. Below shows what to include in this list file:
```
<ul>
{{ range .Pages}}
    <li>
        <a href ="{{.Permalink}}">{{.Title}}
        </a>  
    </li>
{{end}}
</ul>
```
- This has the similar logic to the auto sub-section linking we did
- To create the Table of Contents on a page one can write the below command below or above     `{{ .Content }}` in a markdown file.
```
    <aside>
        {{ .TableOfContents }}
    </aside>
```
### 9. Image optimization for web
- I'll write more on this later, but for now:
    - I have edited all the images involved within my webpage to be optimized for web using imagemagick software.
    - It was easy to use, some of the commands and options I used were: 
        `convert`
        `identify`
        `mogrify`
        `-resize`
        `-crop`
        `-quality` 
- Examples: 
    - magick mogrify -format jpg *.png
    - magick mogrify -resize 50% rose.jpg
    - magick convert preview.jpg -crop 1066x800-150+300 -quality 50 -scale 50% preview2.jpg 
    - magick convert test.jpg -scale x400 test2.jpg
    - magick identify preview2.jpg 

### 10. Adding pictures within HUGO
- I'll write more on this later, but for now:
    - Most basic way of doing this is just put a markdown style table and put the image name and image text in it. Example:
    `![Notebook_page_screenshot](notebook_page.png )`
- To make the images edited all together with css we can add a `style.css` file under `static/assets/css`like below:
```
body {
    background: yellow
    img{
        width = 100
    }
}
``` 
- Then you would want to add this within `head.html`
```<link rel="stylesheet>" hfref="{{.Site.BaseURL}} assets/css/style.css">```

### 11. Video optimization for web
- Since I did not need to add any video content up to now, I will add a short video of my cat here to demonstrate I am able to edit videos with ffmpeg as well.
- I used `ffprobe` to see the properties of the video.
- My video was already in `.mp4` format but to convert a non-mp4 file to mp4 we can use the command below:
   `ffmpeg -i rawvideo.mov rawvideo.mp4`
- We should use libx264 compression and conver our videos to mp4.
- We should keep the video sizes around 9 MB for a minute and less than a MB for 10 seconds of content.
- I used the command below to compress, change quality, scale (keeping aspect ratio), mute and copy:
    `ffmpeg -i trial_video.mp4 -c:v libx264 -crf 25 -vf scale=352:-1 -an -c:a copy trial_video_2.mp4`
- Now I will add the video here, but for that I need to create shortcodes within Hugo - next paragraph

### 12. Adding videos within HUGO
- So first I'll go to layouts, create shortcodes folder, create a video.html file under it
- I copied this code to be put in video.html from https://roneo.org/en/hugo-create-a-shortcode-for-local-videos/ 
```
<video class="video-shortcode" preload="{{ .Get "preload" }}" controls>
    <source src="{{ .Get "src" }}" type="{{ .Get "type" }}">
    Video
</video>
```
- Here preload means it'll preload before the page, controls allows us to have the video controls on the screen, otherwise it becomes a gif
- Now we can use this shortcode for adding our video to here like below within `{{}}` tags:
`< video src="trial_video.mp4" type="video/mp4"  preload="auto" >`

{{< video src="trial_video.mp4" type="video/mp4" preload="auto" >}}

### 13. Using Bootstrap in HUGO
- This was actually a bit confusing but the general idea I got is:
    - Download bootstrap
    - Put js and css files to correct location - Example: `static/assets/css/bootstrap.min.css`
    - Go to `head.html`and put the comment below for configuring javascript
    `<link rel="stylesheet" href = "{{ .Site.BaseURL }}assets/css/bootstrap.min.css">`
    - Go to .html files (singles, index and nav) and add classes everywhere to link the correct styles: stlyes could be dropdown menus, styling an image and etc.
    - I just followed Kris' main instructions and decided not to go depper on this because it will take too much time and complicate the code a lot.
    - The page currently works and has a nice basic look for it, and I am happy with it!
- Here is the final look of the main page with bootstrap:
![Main_(Index)_page_after_bootstrap_in_local](main_page.jpg "Neat")

### 14. . gitignore
- We can create a `.gitignore` file in `HUGO` directory to write file names to be ignored by git