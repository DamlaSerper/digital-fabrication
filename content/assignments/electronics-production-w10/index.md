+++
    Title = "Electronics Production - Week 10"
    weight = 100
+++
## Some summary from today's session
I did minor size improvements and moved the engraving text around in KiCad after some unsuccessful PCB milling trials. 

I also noticed some of the letters are not recognized by CopperCAM (E, A, H, T), therefore I switched them to their lower-case version and only letter "t" gave me a problem.

The problem with the above letters seems to be the overlapping lateral parts in each (–), this "–" is recognized as a pad rather than a track/line, therefore it is not possible to turn it into an engraving.

Instead I disabled "–" pad and continued cutting this way and ended up with an "l".

Also the PCB mill's board is not level enough, for me only right bottom corner gave a good results, with medium spindle speed and 0.08 engraving depth.

Needed to increase drill size and copper layer dimension in CopperCAM, since rivets need to fit in (rivet inner diameter 1 mm, outer dimater 1.4 mm, KiCad drawing hole size 1 mm).

### Copper CAM (for KiCad PCB updates and Gerber files see also Week 8 Trial II)
First thing to do is to open your files in CopperCAM, for this open the software and click File, Open, New circuit. Here you can select the F.Cu layer Gerber file to start.

![Menu](I1.JPG)

![Menu](I0.JPG)

Later select one of the inner lines that is going to be an inner edge cut, right click and select define as card countour. This prompts another window where you should select "cut path is" as "inside". You might need to do this multiple times if there are multiple cuts within the board.

![Menu2](I2.JPG)

![Menu3](I3.JPG)

Then select one corner of the text to be engraved and right click. Select engrave tracks as centerlines.

![Menu4](I4.JPG)

Then disable the erronous letter parts that show up as pads by right clicking and selecting disable pad.

![Menu5](I5.JPG)

Now it is time to import the drill gerber files. Click file, open drills and select the PTH drill gerber file.

![Menu6](I6.JPG)

![Menu7](I7.JPG)

After imprting the holes you will noticed they might be positioned in a wrong orientation and scale. If you need rotate them select file, rotate 90 degree until you reach the correct orientation.

![Menu8](I8.JPG)

After getting the correct orientation to correct the scale of distance between the holes, select layer 1. Select one of the drill holes in the copper layer drawing and right click. Select this pad as reference pad 1. Then select layer 5 (only drill hole layer), select one of the drill holes that are disoriented and right click. Select adjust to reference pad #1. Then repeat this for a second drill hole. Tip the second one needs to be select as second reference pad.

![Menu9](I9.JPG)

![Menu10](I10.JPG)

![Menu11](I11.JPG)

![Menu12](I12.JPG)

![Menu13](I13.JPG)

![Menu14](I14.JPG)

After this you can modify the size of the these drill holes by right clicking them and selecting change diameter. I changed the diameter to 1.5 to fit the 1.4 mm outer diameter rivets that will house 1 mm-ish cable connections. I also changed the outer copper layer diameter to 2 mm.

![Menu15](I15.JPG)

![Menu16](I16.JPG)

Now we can add the dimension of the board by selecting file, dimensions and say just okay to automatically update the dimensions to fit the board. you can set-uo the thickness of the board to 1.8 mm (measurement with caliper 1.7 mm for board plus double sided tape)

![Menu17](I18.JPG)

![Menu18](I18.JPG)

We will also need to set the origin, select file, origin and enter 0, 0 to X and Y directions respectively.

![Menu19](I19.JPG)

![Menu20](I20.JPG)

![CopperCAM](CopperCam.JPG)

Finally we can select the tools to be used and their settings, for this click on parameters and selected tools. We will use the same tool for cutting and drilling (0.8 mm tool -> select use single tool for all drills) and another drill bit for engraving (60 degree tool). Here the engraving depth is selected as 0.08 mm (reduced from 0.1 after harsh/deep engraving). The cutting and drilling depth is selected as 1.9 mm (Board plus double sided tape is 1.7 mm (measured with caliper) + 2 mm). Engraving speed is set at 8 mm/s.

![Menu21](I21.JPG)

![Menu22](I22.JPG)

![Menu23](I23.JPG)

After selecting the tools we can create the contour lines. For this I used only 2 consecutive contours and 0 additional contours around the pads due tight deisgn of my circuit board. You can use the button under the contour line menu to disable to contours, if you want to change the number of contours. You can aslo click on the orange icon in order to see contour or board view.

![Menu24](I24.JPG)

![Menu25](I25.JPG)

![Menu26](I26.JPG)

![Menu27](I27.JPG)

![CopperCamPaths](CopperCamPaths.JPG)

Now its finally time to export these CopperCAD files (.egx). For this click on the milling icon and it will prompt the output screen. In this output screen select all the operations to be done, order is irrelevant, and select the z zero point to circuit surface and X-Y zero point to whte cross. You can also modify the cutting and engraving speeds here again. The output files get automatic extensions of T1 (for specific engraving tool) and T2 (for specific drilling tool).

![Menu28](I28.JPG)

![Menu29](I29.JPG)

![Menu30](I30.JPG)

### VPanel for SRM-20 and setting up the machine
The workflow for SRM-20 machine is easy.

Stick your board on to the platform with double sided tape.

Open VPanel software.

Lift the machine lid and loosen the nut that keeps the drill bits in place.

Insert your choice of drill bit (aka engraving or drilling tool), push it all the way up and fasten the nut.

Using the VPanel software UI, position the X and Y so that it is at the center of your geometry to be milled.

Then start lowering the Z axis until quite close but not all the way. (Look at the marked part in the image)

![Roland](Roland.JPG)

At this point, loosen the bit gently while holding it until it touches the ground. Tighten it here while holding it.

Finally press Z to complete Z axis calibration and slightly lift the bit in the upper Z direction to prevent damage to the bit.

Continue to X and Y calibration by moving to the left bottom corner of the part you will be cutting (left bottom because thats how we set it at the CopperCAM).

Press X-Y when you are satisfied with the location.

Finally adjust the spindle speed and select cut.

Delete any existing files.

Select add and add your file (T1 for engraving, T2 for drill/cut).

Select output and resume which will start the milling process.

After satisfactory results move to other operations (for example if you did engraving now cutting and drilling). You only need to change the tool bit and readjust Z calibration. (No need for recalibrating X-Y) Also do not forget to select the new cut file.

![Menu31](I31.JPG)

![Menu32](I32.JPG)

{{< video src="video.mp4" type="video/mp4" preload="auto" >}}

### Installing rivets
In my design there is need for rivets to prevent bulky pin connections. 

Rivets are first put on top of the hole with the help of a tweezer and pushed into the front side using a rivet pusher (smaller one) and hammer (do this on the gray mat). After the rivet goes all the way through, rotate its back and put it on the metal clamp surface. USe the bigger rivet pusher and hammer to flatten to back side of the rivet.

![Menu33](I33.JPG)

![Menu34](I34.JPG)

![Menu35](I35.JPG)

### Soldering cables

Soldering cables was a disaster but after discussing with Solomon I mamaged to solder one of the connecter pin sockets to top holes. This resulted in a way better connection than my initially flimsy soldering job.

### Soldering LEDs, resistors and MOSFET

LED's were the hardest to solder due to not having any protruding conductive parts. While at the beginning I hated this process, at the end I managed to produce and ugly looking functional board.

### Electrical connection check with multimeter
Multimeter is very important here, because I used it to check after milling if the tiny copper connections are still connective, are there any areas that shouldn't be connected that are connected and etc. Also during soldering to ensure the same things. For this I put the multimeter in the connectivity mode with beeping (the wireless like symbol). This allow me to test tings out.

I used the LED test of the multimeter as well to see if the LED's are connected well wnough to produce full brightness.

I used the resistance mode of the multimeter to measure resistance of the resistors and trimmer potentiometers.

I used the DC mode of the multimeter to check if Raspberry pi is supplying the voltage to the board.

![Multimeter](Multimeter.jpg)

### RPi programming of the LED hat (gpiozero library)

Here is the simple code I wrote to test out functionality of the LED hat and camera.

![Code](code.jpg)

### Results

Here is the final board:

![Final](LEDHAT.jpg)

And its video:

{{< video src="LED_HAT.mp4" type="video/mp4" preload="auto" >}}

### Conclusion

Soldering is very hard, but practice makes perfect.