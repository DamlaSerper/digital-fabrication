+++
    Title = "Final_Project"
+++

# FINAL PROJECT

My final project will be building a functional camera case for a Raspberry Pi Camera (v3, wide angle). The need for this arose from my PhD topic, where I need to take video images of a very high speed chemical process machinery. The space for fitting the camera tight and there is no light within the basket. There are only two variables to play with in addition to the camera case size, which is the thickness of the pipe where this camera will be attached to and the anfle it is attached at. You can see below 2D image of the machinery and where I wish to fit the camera case.

|![Centrifugal filter 2D sketch](Centrifugal_Filter_2D_Sketch.jpg)|
|--|

 Kris already helped me before to create an initial prototype of such case and an LED hat for Raspberry Pi, although there are some improvements needed in order to fit and get full field of view needed for analysis. The case was composed of multiple component that are sketched below.

|![Camera case with led hat](FinalProjectSketch.jpg)|
|--|

### Things to be build in FABLAB:
- LED hat version 2 (Circuit board design, PCB milling and soldering - Due to potential camera size change)
- Camera case version 2(SLA printing- after 3D modelling and FDM prototyping - Pipe size changed and camera size changed)
- Acrylic front class version 2 (Laser cutter - Material could be changed)
- Silicon O-rings (Silicon Moulding - For waterproofing the case and reducing vibrations)
- Pipe version 2 (SLA printing- after 3D modelling and FDM prototyping - The diameter could be thinned to as low as 1.5 cm)
- Connection between camera case and pipe (SLA printing- after 3D modelling and FDM prototyping - Will look for alternatives, easy breakage)

### Already have:
- Version 1 prototypes
- Centrifuge (chemical engineering machinery)
- Raspberry Pi
- Raspberry Pi Camera v2 Noir
- Tough and hard resin + resin tank
- PETG filament

### Needs ordering:
- Raspberry Pi Camera v3 wide angle  (This one is able to autofocus and is wide angle)

### The demanding requirements for this project:
- The camera needs to fit to the area (volume) marked on the first image
- The camera, LED hat, acrylic plate and silicon moulds have their own minimum size limitation
- Camera should not be submerged into the basket
- Camera should be water and chemical proofed
- Camera should be able to capture in focus video of the marked area of interest (angle needs to be right)
- Pipe could be minimum 1.5 cm thin
- Camera should be connected in such a way to the pipe that the vibration is minimal due to rotation
- Same applies for the new pipe
- Parts should be removable
- Electric cables and PCB's to be somehow protected (coating?)