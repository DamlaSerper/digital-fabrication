+++
    Title = "About"
+++

# ABOUT ME

My name is Damla Serper. I study chemical process engineering and am currently a doctoral researcher at Aalto CHEM. I am interested in learning fast prototyping to design imaging system for chemical process machinery and to satisfy my own curiosity. Some personal facts: I like watercolor painting, crafting and boardgames. Below you can see me and my cat!

|![Picture of me](human.jpg "Hooman")|![Picture of my cat](cat.jpg "Rebecca")|
|--------------------|:---------------------|