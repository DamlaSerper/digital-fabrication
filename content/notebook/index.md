+++
    Title = "Notebook"
+++

# NOTEBOOK
You can write your notes here!

## Git commands

`git init` (to initialize a git directory

`ls -a` (to see .git hidden folder in the terminal)

`git status` (to see the status of the files within the current directory/ 
Are they committed or shelving?)

`git add` * (*= "file name" to add the file to a shelving area)

`git add` -A (to add all files to shelving area)

`git commit -m '*'` (*= 'message/comment' to commit shelved files and add a comment)

`git log` (to see the history of git repository/ Who committed what and when?)

`touch *` (*="file name" to change the timestep of a file, it creates a file if the file doesn't already exist)

`git diff` (to see what changed in the commited file(s) before re-adding to shelving area, after a change)

`q` (to exit some of the git windows) git config --global user.name "*" (="Name Surname" to change the global user name)

`git config --global user.email "*"` (="email" to change the global user email)

`git clone git@gitlab.com:DamlaSerper/digital-fabrication.git`(to clone the said directory to a new local place)

`git pull git@gitlab.com:DamlaSerper/digital-fabrication.git master` (to get the new changes from online server master branch to our local - need to do this before trying to push anything new)

`git remote add origin git@gitlab.com:DamlaSerper/digital-fabrication.git` (to add a remote to your local computer, do this if it starts asking about the password and key, git address needed not the html)

## GitLab CI file

`pages:` (when GitLab robot sees this, it picks up this code)

(This will allow you to deploy your website to GitLab servers)

`  stage: deploy` (launches a docker image, like a cloud computer, that 

will try to install mini Linux)

`  script:` (lines to be executed)

`    -mkdir .public` (. stands for hidden in Linux)

`    -cp -r * .public` (copy the directory)

`    -mv .public public` (rename hidden public to visible public)

`  artifacts:` (these will be the file you push to the git repository)

`    paths:` (paths to those)

`      public` (artifaccts will be public)

`  only:` (which branch)

`    -master` (master branch)

(You can also create this file from the web interface og GitLab but don't 
foget to git pull in local afterwards)

After getting on with HUGO, you should create the HUGO version of this file using the template on the website and copy it to your local repository existing ci.myl file to save and update to go to HUGO from just HTML.